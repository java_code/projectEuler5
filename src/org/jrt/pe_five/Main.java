/**
 * org.jrt.pe_five is a list of operations to complete project euler problem five
 */

package org.jrt.pe_five;

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * 
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 *  
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {
		int largestNum = 20;
		int numTest = largestNum;
		boolean foundNum = false;
		
		while (true) {
			for(int i = largestNum; i > 0; i--) {
				if(numTest % i != 0) {
					break;
				}
				if(i == 1) {
					foundNum = true;
				}
			}
			if(foundNum == true) {
				break;
			}
			numTest += largestNum;
		}
		
		System.out.println(numTest);
	}

}
